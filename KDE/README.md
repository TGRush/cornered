# cornered for KDE Plasma

This is somewhat WIP.

## Installation
1. Copy `Cornered.colors` to `~/.local/share/color-schemes/`
2. Apply the colorscheme from the settings.

### Note: If you see inaccurate accents, simply overwriting the accent from settings should make for a nice workaround until I properly fix it.
