# cornered

A sleek, dark color scheme. Credits to [this guy!](https://www.reddit.com/r/unixporn/comments/ilnlju/openbox_everywhere_i_turn_i_find_a_corner/)

<img title="" src="https://codeberg.org/TGRush/cornered/raw/branch/main/cornered-screenshot.png" alt="" data-align="center">

## Why? What?

I've found this post thanks to a user on the Catppuccin Discord, and I couldn't pass the opportunity to properly making this into a color scheme. Thus, I've extracted as many colors as I could, and am gathering all the information I have here to make this become a full color scheme.

## Contributing

1. Fork this repository
2. Add your desired port to .git-modules
3. Create a pull request

From there, I review your PR and decide whether to add it or not. Usually PRs go through, unless they're absolute bogus.

## Ports

**Check the folders of the repo above! It's where ports are found ;)**

## Credits

Thanks a lot to the [OP on Reddit!](https://www.reddit.com/r/unixporn/comments/ilnlju/openbox_everywhere_i_turn_i_find_a_corner/), wouldn't have been possible without them. Sadly, the dotfiles of the original are gone, and the Reddit account is deleted too :/
This Reddit thread is also where the screenshot at the top of this README is from.

## Colors

### Normal Variants

| Color                          | HEX       | RGB                  | HSL                  |
|:------------------------------:|:---------:|:--------------------:|:--------------------:|
| Black/Base                     | `#050505` | `rgb(5, 5, 5)`       | `hsl(0, 0%, 2%)`     |
| White/Text                     | `#F0F0F0` | `rgb(250, 250, 250)` | `hsl(0, 0%, 98%)`    |
| Blue                           | `#898cab` | `rgb(137, 140, 171)` | `hsl(235, 17%, 60%)` |
| ~~Light Blue~~ another green   | `#89a1ab` | `rgb(137, 161, 171)` | `hsl(198, 17%, 60%)` |
| Teal                           | `#8AABAC` | `rgb(138, 171, 172)` | `hsl(182, 17%, 61%)` |
| Pink/Magenta                   | `#AC8AAC` | `rgb(172, 138, 172)` | `hsl(300, 17%, 61%)` |
| Purple                         | `#8F8AAC` | `rgb(143, 138, 172)` | `hsl(249, 17%, 61%)` |
| Yellow                         | `#ACA98A` | `rgb(172, 169, 138)` | `hsl(55, 17%, 61%)`  |
| Green                          | `#8AAC8B` | `rgb(138, 172, 139)` | `hsl(122, 17%, 61%)` |
| Red                            | `#AC8A8C` | `rgb(172, 138, 140)` | `hsl(356, 17%, 61%)` |

### Lighter Variants

| Color                          | HEX       | RGB                  | HSL                  |
|:------------------------------:|:---------:|:--------------------:|:--------------------:|
| Black/Base                     | `#0E0E0E` | `rgb(12, 12, 12)`    | `hsl(0, 0%, 5%)`     |
| White/Text                     | `#FAFAFA` | `rgb(250, 250, 250)` | `hsl(0, 0%, 98%)`    |
| Blue                           | `#7a7d9a` | `rgb(122, 125, 154)` | `hsl(235, 13%, 54%)` |
| ~~Light Blue~~ another green   | `#7a9099` | `rgb(122, 144, 153)` | `hsl(198, 13%, 54%)` |
| Teal                           | `#9EC3C4` | `rgb(158, 195, 196)` | `hsl(182, 24%, 69%)` |
| Pink/Magenta                   | `#C49EC4` | `rgb(196, 158, 196)` | `hsl(300, 24%, 69%)` |
| Purple                         | `#A39EC4` | `rgb(163, 158, 196)` | `hsl(248, 24%, 69%)` |
| Yellow                         | `#C4C19E` | `rgb(196, 193, 158)` | `hsl(55, 24%, 69%) ` |
| Green                          | `#9EC49F` | `rgb(158, 196, 159)` | `hsl(122, 24%, 69%)` |
| Red                            | `#C49EA0` | `rgb(196, 158, 160)` | `hsl(357, 24%, 69%)` |
